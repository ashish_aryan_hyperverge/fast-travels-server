const errors = require("../utils/errorUtil");

function validation(req, res, next) {
    if (req.originalUrl === '/api/user/signup') {
        const { name, password, phone } = req.body;
        if (!name || !password || !phone) {
           return next(errors.bad_request("Bad request"));
        } 
    }
    else if (req.originalUrl === '/api/user/login') {
        const { phone, password} = req.body;
        if (!phone || !password) {
            return next(errors.bad_request("Bad request"));
        } 
    }
    else if (req.originalUrl === '/api/bus/all' ) {
        const { sourceCity, destinationCity, travelDate} = req.body;
        if (!sourceCity || !destinationCity || !travelDate) {
            return next(errors.bad_request("Bad request"));
        }
    }
    else if (req.originalUrl === '/api/ticket/booking') {
      
      
        const {   bus_id, ticket_no, name, age, phone} = req.body;
        if (!bus_id || !ticket_no || !name || !age || !phone) {
            return next(errors.bad_request("Bad request"));
        }
    }
    else if (req.originalUrl === '/api/bus/addbus') {
        const { name, source, dest, cost, bus_no, date, boarding_time, dropping_time} = req.body;
        if (!name || !source || !dest || !cost || !bus_no || !date || !boarding_time || !dropping_time) {
            return next(errors.bad_request("Bad request"));
        }
    }
    next();
}

module.exports = validation;