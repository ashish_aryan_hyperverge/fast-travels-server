const jwt = require('jsonwebtoken');
const errors = require('../utils/errorUtil');
const isAdmin = async (req, res, next) => {
    var token = req.headers['x-access-token'];
	jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
		if (err) {
			console.log('error found');
			return next(errors.unauthorised('user not authorised'));
		}
		if (!data.isAdmin) {
			return next(errors.unauthorised('user not authorised'));
		}
		next();
	});
};

module.exports = isAdmin;
