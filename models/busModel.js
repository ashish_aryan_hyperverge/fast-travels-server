const mongoose = require("mongoose");
const { ticketSchema, newTicket, Ticket } = require("./ticketModel");

const busSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  source: {
    type: String,
    required: true,
  },
  dest: {
    type: String,
    required: true,
  },
  cost: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  boarding_time: {
    type: String,
    required: true,
  },
  dropping_time: {
    type: String,
    required: true,
  },
  tickets: [ticketSchema],
  bus_no: {
    type: String,
  },
  availability: {
    type: Number,
    default: 40,
  },
});

busSchema.methods.updateTicket = async function (name, age, phone, ticket_no) {
  try {
    if (!this.tickets[ticket_no - 1].isAvail) {
      throw new Error("Already booked");
    }
    this.availability = this.availability - 1;
    this.tickets[ticket_no - 1].name = name;
    this.tickets[ticket_no - 1].age = age;
    this.tickets[ticket_no - 1].phone = phone;
    this.tickets[ticket_no - 1].isAvail = false;
    let result = await this.save();
    return result;
  } catch (err) {
    throw new Error("Already booked");
  }
};

exports.Buses = mongoose.model("bus", busSchema);
