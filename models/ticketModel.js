const mongoose = require("mongoose");
const { Schema } = require("mongoose");

const ticketSchema = new Schema({
  user_id: {
    type: String,
  },
  name: {
    type: String,
  },
  age: {
    type: String,
  },
  phone: {
    type: String,
  },
  ticket_no: {
    type: String,
    // required: true,
  },
  isAvail: {
    type: Boolean,
    default: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
});

exports.ticketSchema = ticketSchema;
exports.Ticket = mongoose.model("ticket", ticketSchema);
