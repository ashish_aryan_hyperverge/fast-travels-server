const User = require("../models/user_model");
const errors = require("../utils/errorUtil");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.signup = async (req, res, next) => {
  const { name, phone, password } = req.body;
  User.findOne({ phone: phone }).then(async (data) => {
    if (data) {
      next(errors.unauthorised("Phone number already in use."));
    } else {
      try {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(password, salt);
        const user = await User.create({
          name: name,
          password: hash,
          phone: phone,
          isAdmin: false || req.body.isAdmin,
        });
        res.status(200).send(user);
      } catch (err) {
        next(errors.internal_server_error("internal server error"));
      }
    }
  });
};

exports.signin = async (req, res, next) => {
  const { phone, password } = req.body;
  try {
    const user = await User.findOne({ phone: phone });
    if (!user) throw new Error("No user found");
    let isValid = bcrypt.compareSync(password, user.password);
    if (isValid) {
      let token = jwt.sign(
        {
          name: user.name,
          phone: user.phone,
          _id: user._id,
          isAdmin: user.isAdmin,
        },
        process.env.JWT_SECRET
        , {
          expiresIn: 86400000 * 15 // 15 days
        },
      );
      res.status(200).send({user:user,token:token});
    }
  } catch (err) {
    next(err);
  }
};

exports.getUser = (req, res, next) => {
  res.status(200).send(req.user);
};
