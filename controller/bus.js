const { resetTickets, emptyTicket } = require("../utils/ticketUtil");
const {Buses} = require("../models/busModel")
const errors = require("../utils/errorUtil");

exports.getAllBus = async (req, res, next) => {
  try {
    const allBus = await Buses.find({source: req.body.sourceCity, dest: req.body.destinationCity, date: req.body.travelDate });
    res.status(200).send(allBus);
  } catch (err) {
    console.log(err);
    next(errors.internal_server_error("internal server error"));
  }
};

exports.addBus = async (req, res, next) => {
  try {
    let ticket = new Array();
    for (i = 1; i <= 40; i++) {
      ticket.push(emptyTicket(i));
    }
    console.log(req.body.source);
    let bus = await Buses.create({
      name: req.body.name,
      source: req.body.source,
      dest: req.body.dest,
      cost: req.body.cost,
      bus_no: req.body.bus_no,
      date: req.body.date,
      boarding_time: req.body.boarding_time,
      dropping_time: req.body.dropping_time,
      tickets: resetTickets(),
    });
    return res.status(200).send(bus);
  } catch (err) {
    console.log(err);
    next(errors.internal_server_error("internal server error"));
  }
};

exports.resetBus = async (req, res, next) => {
  try {
    let bus_id = req.params._id;
    let bus_ = await Buses.findById(bus_id);
    if (!bus_) next(errors.bad_request("bus not found"));
    bus_.tickets = resetTickets();
    bus_.availability = 40;
    let updated = await bus_.save();
    return res.status(200).send(updated);
  } catch (err) {
    console.log(err);
    next(errors.internal_server_error("server error while ticket reset"));
  }
};


// exports.getAllBus = (req, res, next) => {};

// exports.getAllBus = (req, res, next) => {};
