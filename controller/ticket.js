const { Buses } = require("../models/busModel");
const errors = require("../utils/errorUtil");

exports.newBooking = async (req, res, next) => {
  let { bus_id, ticket_no, name, age, phone } = req.body;
  try {
    console.log("inside ticket controller");
    let result = await Buses.findById(bus_id);
    if (!result) next(errors.bad_request("No such buses available"));
    let response = await result.updateTicket(name, age, phone, ticket_no);
    return res.status(200).send(response);
  } catch (err) {
    console.log(err);
    next(
      errors.internal_server_error(
        "internal server error while booking new ticket"
      )
    );
  }
};

