const router = require("express").Router();
const isAdmin = require("../middlewares/isAdmin");
const validation = require("../middlewares/validation");
const { getAllBus, addBus, resetBus } = require("../controller/bus");

router.post("/all", validation, getAllBus);
router.post("/addbus",validation, isAdmin, addBus);
router.get("/reset/:_id",isAdmin, resetBus);

module.exports = router;
