const router = require("express").Router();
const { newBooking } = require("../controller/ticket");
const validation = require("../middlewares/validation");

router.post("/booking",validation, newBooking);

module.exports = router;
