const router = require('express').Router();
const { signup, signin, getUser } = require('../controller/user');
const validation = require("../middlewares/validation");

router.get('/login', getUser);
router.post('/login', validation, signin);
router.post('/signup', validation, signup);

module.exports = router;
