const express = require("express");
const bodyParser = require("body-parser");
const errors = require("./utils/errorUtil");
const userRoute = require("./routes/userRoutes");
const ticketRoute = require("./routes/ticketRoutes");
const mongoose = require("mongoose");
var cookieParser = require("cookie-parser");
const busRoute = require("./routes/busRouter");
const dotenv = require("dotenv");
dotenv.config();
const InitiateMongoServer = require("./config/db");
InitiateMongoServer();

const app = express();
app.use(cookieParser());
if (dotenv.error) throw new Error("Error in fetching .env file");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/user", userRoute);
app.use("/api/bus", busRoute);
app.use("/api/ticket", ticketRoute);

app.get("/*", (req, res, next) => {
  console.log("page not found");
  res.send({ msg: "page not found" });
});

app.use((err, req, res, next) => {
  console.log("error is called", err);
  res.status(err.statusCode).send(err);
});

var port = 8000; 

app.listen(port, () => {
    console.log('Bus Ticket Booking API is listening on port ' + port);
});
