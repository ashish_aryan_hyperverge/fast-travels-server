
const { ticketSchema, Ticket } = require("../models/ticketModel");

  const emptyTicket = function (ticketNumber) {
    return new Ticket({
      name: "",
      age: "",
      phone: "",
      ticket_no: ticketNumber,
      status: "available",
    });
  };
  
  const resetTickets = () => {
    let ticket = new Array();
    for (i = 1; i <= 40; i++) {
      ticket.push(emptyTicket(i));
    }
    return ticket;
  };

  const newTicket = (ticketNumber) => {
    return new ticketSchema({
      name: "",
      age: "",
      phone: "",
      ticket_no: ticketNumber,
      status: available,
    });
  };
  
  
 
  exports.newTicket = newTicket;

exports.emptyTicket = emptyTicket;
exports.resetTickets = resetTickets;